测试对数据库，NOSQL，和大数据SQL查询引擎的支持,对于在2.0中验证过得不会在工程里再做验证

# 传统数据库
* MySql 支持
* Postgres 支持
* Mariadb 支持
* SQL Server 支持
* Oracle 支持
* DB2 支持
* H2 支持
* Apache Derby 支持
* SQLLite 支持
* 达梦(国产) 支持
* 神通(国产) 支持
* 人大金仓  支持
* 华为高斯 支持
* DerbyStyle 支持
* 阿里PolarDB

# 物联网时序数据库

* MachBase(韩国） 支持
* 松果（国产)  研发中，我用mac但松果只支持window
* TDengine（国产)  支持

# 大数据

* ClickHouse 支持
* ElasticSearch 研发中，只有ES专业版才支持JDBC，，谁能贡献一下
* HBase 支持
* Hive 支持
* Apache Cassandar 支持


# SQL查询引擎

* Presto 支持
* Drill 支持
* Druid  支持

# 内存数据库

* Couchbase 支持
* Ignite 支持
* voltdb 研发中

# 图&文档库

* neo4j 研发中心

BeetlSQL 如何支持跨数据库操作？
